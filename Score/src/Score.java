import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Score {

    public static void main(String[] args) throws IOException {

        Score score = new Score();
        HashMap<String, Integer> small = score.parseHtml("small.html");
        HashMap<String, Integer> all = score.parseHtml("all.html");
        small.forEach((key, value) -> all.merge(key, value, (v1, v2) -> v1 + v2));
        
        Properties pro = new Properties();
        pro.load(new FileInputStream(new File("total.properties")));
        int befores, bases, tests, programs, adds;
        befores = Integer.parseInt(pro.getProperty("before"));
        bases = Integer.parseInt(pro.getProperty("base"));
        tests = Integer.parseInt(pro.getProperty("test"));
        programs = Integer.parseInt(pro.getProperty("program"));
        adds = Integer.parseInt(pro.getProperty("add"));
        //获取配置文件的key值
        int before = all.get("before");
        int base = all.get("base");
        int test = all.get("test");
        int program = all.get("program");
        int add = all.get("program");
        double gredit = 0;
        //初始化分数
        gredit += before * 1.0 / befores * 100 * 0.25;
        gredit += base * 1.0 / bases * 95 * 0.3;
        gredit += test * 1.0 / tests * 100 * 0.2;
        gredit += program * 1.0 / programs * 95 * 0.1;
        gredit += add * 1.0 / adds * 90 * 0.05;
        int t = (int) (gredit * 100);
        System.out.printf("%.2f",(double) t / 100 + 6);
    }
    //按照计算工式计算分数
    private HashMap<String, Integer> parseHtml(String html) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        String[] strings = {"before", "base", "test", "program", "add"};
        for (String string : strings) {
            hashMap.put(string, 0);
        }

        try {
            Document document = Jsoup.parse(new File(html), "UTF-8");
            Elements select = document.select(".interaction-name");
            for (Element e : select) {
                if (e.attr("title").contains("课堂小测")) {
                    countScore(e, hashMap, "test");
                } else if (e.attr("title").contains("编程题")) {
                    countScore(e, hashMap, "program");
                } else if (e.attr("title").contains("课堂完成部分")) {
                    countScore(e, hashMap, "base");
                } else if (e.attr("title").contains("附加题")) {
                    countScore(e, hashMap, "add");
                } else if (e.attr("title").contains("课前自测")) {
                    countScore(e, hashMap, "before");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return hashMap;
    }
    //与网页中的模板相匹配，并计算出相应的分数
    private void countScore(Element e, HashMap<String, Integer> hashMap, String key) {
        int temp = 0;
        String text = e.parent().lastElementSibling().child(0).select("span:contains(经验)").select("span[style=color:#8FC31F;]").text();
        Matcher matcher = Pattern.compile("\\d+").matcher(text);
        while (matcher.find()) {
            String group = matcher.group();
            temp += Integer.parseInt(group);
        }
        hashMap.put(key, hashMap.get(key) + temp);
    }
}
